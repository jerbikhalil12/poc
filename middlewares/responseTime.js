const os = require("os");
const influx = require("../config/db");
module.exports = (req, res, next) => {
  const start = Date.now();

  res.on("finish", () => {
    const duration = Date.now() - start;
    console.log(`Request to ${req.path} took ${duration}ms`);

    influx
      .writePoints([
        {
          measurement: "co2_consumption",
          tags: { host: os.hostname() },
          fields: { duration, path: req.path }
        }
      ])
      .catch(error =>
        console.error(`Error saving data to InfluxDB! ${error.stack}`)
      );
  });
  return next();
};
