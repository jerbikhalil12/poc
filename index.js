const express = require("express");
const dotenv = require("dotenv");
const app = express();

// load env vars
dotenv.config({ path: "./config/config.env" });

// middleware to catch response time and save to db
const responseTime = require("./middlewares/responseTime");

// route import
const route = require("./routes/route");

const PORT = process.env.PORT || 5000;

// db
const influx = require("./config/db");

influx
  .getDatabaseNames()
  .then(names => {
    if (!names.includes(process.env.DB_NAME)) {
      return influx.createDatabase(process.env.DB_NAME);
    }
  })
  .then(() => {
    // middlewares
    app.use(responseTime);

    // routes
    app.use("/api/v1", route);

    app.listen(PORT, () => console.log(`App up on PORT: ${PORT}`));
  })
  .catch(err => {
    console.error(`Error creating Influx database!`);
  });
