const router = require("express").Router();
const os = require("os");
const Influx = require("influx");
const influx = require("../config/db");

router.get("/times", (req, res) => {
  influx
    .query(
      `
      select * from co2_consumption
      where host = ${Influx.escape.stringLit(os.hostname())}
      order by time desc
      limit 10
    `
    )
    .then(result => {
      setTimeout(() => res.json(result), 2000);
    })
    .catch(err => {
      res.status(500).send(err.stack);
    });
});

module.exports = router;
