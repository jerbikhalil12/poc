const Influx = require("influx");

const db = new Influx.InfluxDB({
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  port: process.env.DB_PORT,
  schema: [
    {
      measurement: "co2_consumption",
      fields: {
        path: Influx.FieldType.STRING,
        duration: Influx.FieldType.INTEGER,
      },

      tags: ["host"],
    },
  ],
});

module.exports = db;
